# Alex Mandel and Ani Ghosh
# University of California
# 2017

# Parse xml ODK data into a table for review
# Long term and Shiny app for viewing data/photos/locations might be nice
library(XML)
library(plyr)

# Make a list of the xml files in the correct folders KateAg
# For each xml file parse and clean the structure, then rbind them all together to get nice table

parseODK <- function(input){
  # Read XML document of ODK data collect
  doc <- xmlTreeParse(input, useInternal=T)
  root <- xmlRoot(doc)
  tim <- basename(input)
  h <- substr(tim, regexpr('_', tim)[1]+1,(nchar(tim)-4))
  # Get the names of the fields
  var.names <- names(root)
  # Extract the values of the fields
  dvalues <- sapply(var.names, function(x) {xpathSApply(root, x, xmlValue)})
  #Convert to a data frame
  outrow <- as.data.frame(t(dvalues), stringAsFactors=FALSE)
  outrow$timestamp <- h
  return(outrow)
}

# Load just the KateAg forms
# TODO: take input from use as to which form they want to review
datfiles <- list.files("odk/instances",recursive = TRUE, pattern = "^KateAg.*xml$", full.names = TRUE)

# Convert XML to Data Frames
parsed <- lapply(datfiles, parseODK)
# Merge the data frames
output <- rbind.fill(parsed)

#Split the location field into x,y,z,accuracy
locsplit <-strsplit(as.character(output$Location), split=" ")
locdf <- as.data.frame(do.call(rbind, locsplit))
names(locdf) <- c("lat","long","alt","accuracy")
output <- cbind(output,locdf)

write.csv(output, file="some.csv", row.names = FALSE)
